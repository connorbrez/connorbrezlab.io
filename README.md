# Portfolio  [![Gitlab Pipeline Status](https://gitlab.com/connorbrez/connorbrez.gitlab.io/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/connorbrez/connorbrez.gitlab.io/-/pipelines) [![lighthouse report](https://img.shields.io/badge/lighthouse_report-grey)](https://gitlab.com/connorbrez/connorbrez.gitlab.io/-/jobs/artifacts/master/file/lh-desktop-report.html?job=lighthouse_test) [![code quality report](https://img.shields.io/badge/code_quality_report-grey)](https://gitlab.com/connorbrez/connorbrez.gitlab.io/-/jobs/artifacts/master/file/gl-code-quality-report.html?job=code_quality_test)
### [https://connorbrez.gitlab.io](https://connorbrez.gitlab.io)
#### Made using Go, HTML, CSS and JavaScript.

- Static site generation with templating
- Blog feature generates HTML from markdown files
- Auto rebuild when working locally



