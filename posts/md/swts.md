---
author: Connor Brezinsky
title: Swinging with the Stars
desc: Raising money for Medically Inclusive Childcare, presented by Hopes Home.
thumb: ./posts/img/swts.webp
thumb_alt: Swinging with the Stars logo on a stary background
draft: true
---

{#title}
# Swinging with the Stars
## Raising money for Medically Inclusive Childcare, presented by Hopes Home.
- [Hopes Home](https://hopeshome.org/)

I delivered a web application to help Hopes Home facilitate donations and event tickets for their charity event Swinging with the Stars. It allows them to create events yearly, sell tickets, and track donations per event with simple reporting and analytics. Patrons can donate and can see in real-time what the donation total is. I used Stripe for payment processing, Angular for the front end and Go for the backend services. Initially, I hosted this on GCP App Engine, mainly for scaling and performance reasons. However, I switched over to DigitalOceans App Platform for a more reasonable cost while still maintaining performance, availability, and scalability. This project has helped Hopes Home raise over $400,000 for Medically Inclusive Childcare.

Swinging with the Stars is based on the model from the popular TV show Dancing with the Stars. 8 pairs spend many hours over several months learning and rehearsing a 2-3 minute choreographed routine that they will perform in front of a LIVE audience! The audience members vote for their favorite duo via donation to be crowned the winners! 


