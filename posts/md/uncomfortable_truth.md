---
author: Connor Brezinsky
title: The Uncomfortable Truth
desc: Raising awarness for Uyghur Forced Labor, presented by The Human Rights Foundation
thumb: ./posts/img/uncomfortable_truth.webp
thumb_alt: "The Uncomfortable Truth: There's a 1 in 5 chance your clothes are linked to Uyghur forced labor." 
---

{#title}
# The Uncomfortable Truth
{#subtitle}
## Uyghur Forced Labor Checker
- [Website](https://hrf.org/programs/uncomfortable-truth)
- [Plugin](https://chromewebstore.google.com/detail/uyghur-forced-labor-check/ejodaepockllkcloibcchpjnfoopincp)


I do have to admit, this was a cool project to work on. When I first got the message that this was potentially happening, I was a little intimidated, but I was more excited to work on something meaningful. I think most people in the Western world do know that forced labor still exists. Maybe it's not something we talk about or think about daily, but we should try to be more conscious about understanding where the things we buy come from. This project with The Human Rights Foundation helps bring light to the Uyghur Forced Labor problem in China and puts the spotlight on some brands that you may not know contribute to the problem. 

The Web extension is offered in Google Chrome. When visiting a clothing brand's website it would notify the user if they were a brand that might have ties to Uyghur forced labor, or if they were a brand that has publicly committed to disengaging with facilities and suppliers implicated in Uyghur forced labor. It was released in November 2021 and won multiple Webby awards. 

The project follows Google and Mozilla web extension standards, with an API written in Go. It's currently hosted in the cloud with DigitalOceans App Platform for easy and cost-effective deployments, performance, and scalability.

{#images}
![Uyghur Forced Labor Checker ](../img/labor_checker.webp)