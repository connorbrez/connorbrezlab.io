module connorbrez.gitlab.io/m/v2

go 1.22.0

require github.com/gomarkdown/markdown v0.0.0-20240419095408-642f0ee99ae2

require (
	golang.org/x/sys v0.13.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/fsnotify/fsnotify v1.7.0
	github.com/go-yaml/yaml v2.1.0+incompatible
)
