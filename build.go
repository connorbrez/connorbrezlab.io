package main

import (
	"html/template"
	"io/fs"
	"log"
    "log/slog"
    "os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/go-yaml/yaml"
	"github.com/gomarkdown/markdown"
	"github.com/gomarkdown/markdown/html"
	"github.com/gomarkdown/markdown/parser"

	"github.com/fsnotify/fsnotify"

	"fmt"
)

const MDPostDir = "./posts/md"
const HTMLPostDir = "./posts/html"
const PagesDir = "./pages"
const TemplatesDir = "./templates"

type Post struct {
	FileName string
	HTML     []byte
	Meta     PostMeta
}

type PostMeta struct {
	Author    string `yaml:"author,omitempty"`
	Thumb     string `yaml:"thumb,omitempty"`
	ThumbAlt  string `yaml:"thumb_alt,omitempty"`
	Title     string `yaml:"title,omitempty"`
	Desc      string `yaml:"desc,omitempty"`
	Slug      string `yaml:"slug,omitempty"`
	CreatedAt string `yaml:"created_at,omitempty"`
	UpdatedAt string `yaml:"updated_at,omitempty"`
}

type PostData struct {
	Post        Post
	Title       string
	Description string
	Body        template.HTML
}

func getPosts() []Post {
	entries, err := os.ReadDir(MDPostDir)
	checkError(err)

	posts := make([]Post, 0, len(entries))
	for _, entry := range entries {
		info, err := entry.Info()
		checkError(err)

		var p Post
		p.FileName = info.Name()
		p.Meta = parsePostFrontMatter(p)
		p.HTML = postToHTML(p)

		posts = append(posts, p)
	}
	return posts
}

func getPages() []fs.DirEntry {
	pages, err := os.ReadDir(PagesDir)
	checkError(err)
	return pages
}

func parsePostFrontMatter(p Post) PostMeta {
	md, err := os.ReadFile(fmt.Sprintf("%v/%v", MDPostDir, p.FileName))
	checkError(err)

	var t PostMeta
	err = yaml.Unmarshal(md, &t)
	checkError(err)
	t.Slug = strings.Split(p.FileName, ".")[0]
	return t
}

func postToHTML(p Post) []byte {
	md, err := os.ReadFile(fmt.Sprintf("%v/%v", MDPostDir, p.FileName))
	checkError(err)

	mdSlice := strings.Split(string(md), "---")
	if len(mdSlice) >= 3 {
		return mdToHTML([]byte(mdSlice[2]))
	} else {
		return mdToHTML(md)
	}
}

func mdToHTML(md []byte) []byte {
	extensions := parser.CommonExtensions | parser.NoEmptyLineBeforeBlock | parser.Attributes
	p := parser.NewWithExtensions(extensions)
	doc := p.Parse(md)

	htmlFlags := html.CommonFlags | html.HrefTargetBlank
	opts := html.RendererOptions{Flags: htmlFlags}
	renderer := html.NewRenderer(opts)

	return markdown.Render(doc, renderer)
}

func createPage(templates *template.Template, tmpl string, data interface{}) {
	name := strings.Replace(tmpl, ".tmpl", "", -1)
	htmlFile, err := os.Create(fmt.Sprintf("%v.html", name))
	checkError(err)
	defer htmlFile.Close()

	t, err := templates.ParseFiles(fmt.Sprintf("%v/%v", PagesDir, tmpl))
	checkError(err)
	err = t.ExecuteTemplate(htmlFile, "base", data)
	checkError(err)

}

func createPost(templates *template.Template, data PostData) {
	var path = fmt.Sprintf("posts/html/%v.html", strings.Replace(data.Post.FileName, ".md", "", -1))
	htmlFile, err := os.Create(path)
	checkError(err)
	defer htmlFile.Close()

	err = templates.ExecuteTemplate(htmlFile, "blog-post.tmpl", data)
	checkError(err)
}

func buildSite() {
	templateBuilder := template.New("builder")
	_, err := templateBuilder.ParseGlob("templates/*.tmpl")
	checkError(err)

	if strings.ToLower(os.Getenv("ENV")) == "dev" {
		for _, tmpl := range templateBuilder.Templates() {
			slog.Info("template found", "template", tmpl.Name())
		}
	}

	posts := getPosts()
	pages := getPages()

	var wg sync.WaitGroup
	wg.Add(len(pages))
	wg.Add(len(posts))

	for _, page := range pages {
		go func(name string, posts []Post) {
			c, err := templateBuilder.Clone()
			checkError(err)
			createPage(c, name, struct{ Data interface{} }{Data: posts})
			wg.Done()
		}(page.Name(), posts)

	}

	for _, p := range posts {
		go func(p Post) {
			data := PostData{
				Post:        p,
				Title:       fmt.Sprintf("%v - %v", p.Meta.Title, p.Meta.Author),
				Description: p.Meta.Desc,
				Body:        template.HTML(string(p.HTML)),
			}
			c, err := templateBuilder.Clone()
			checkError(err)
			createPost(c, data)
			wg.Done()
		}(p)
	}
	wg.Wait()
}

func main() {
	if _, err := os.Stat(HTMLPostDir); os.IsNotExist(err) {
		os.Mkdir(HTMLPostDir, os.ModePerm)
		checkError(err)
	}

	folder, err := filepath.Abs(filepath.Dir(os.Args[0]))
	checkError(err)
	err = os.Setenv("PORTFOLIO_ABS_PATH", folder)
	checkError(err)

	buildSite()
	if strings.ToLower(os.Getenv("ENV")) == "dev" {
		slog.Info("Running in dev environment, watching for changes...")
		watchAndBuild()
	}
}

func watchAndBuild() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}

				if event.Has(fsnotify.Write) {
					if strings.Contains(event.Name, ".tmpl") || strings.Contains(event.Name, ".md") {
						slog.Info("rebuilding site...", "modified", event.Name)
						time.Sleep(100 * time.Millisecond)
						buildSite()
					}
				}
			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				checkError(err)
			}
		}
	}()

	err = watcher.Add("./posts/md/")
	if err != nil {
		log.Fatal(err)
	}

	err = watcher.Add("./templates")
	if err != nil {
		log.Fatal(err)
	}

	err = watcher.Add("./pages")
	if err != nil {
		log.Fatal(err)
	}

	<-make(chan struct{})
}

func checkError(err error) {
	if err != nil {
		slog.Error(err.Error())
	}
}
